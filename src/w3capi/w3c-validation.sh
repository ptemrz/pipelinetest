#!/bin/bash
# W3C validator API: https://validator.w3.org/docs/api.html

api="https://validator.w3.org/nu/"

#######
# curl --location --header "PRIVATE-TOKEN: 6HyxN8LURQD31pRewYfs" https://gitlab.com/api/v4/projects/18467552/jobs > .jobs
# sed -z 's/,/,\n/g' .jobs > .jobs.sed

# linesWithId=$(cat .jobs.sed | grep id)

# previousJob=$(echo "$linesWithId" | head -12 | tail -1 | grep -o -e "[0-9]*")

# echo "Previous job: $previousJob"

# curl --output artifacts.zip --location --header "PRIVATE-TOKEN: 6HyxN8LURQD31pRewYfs" https://gitlab.com/api/v4/projects/18467552/jobs/${previousJob}/artifacts/

#unzip artifacts.zip
#######

commitFiles=$(git diff-tree --no-commit-id --name-only -r $CI_COMMIT_SHA)
#files=$(tree -fi --noreport)
files=$(git ls-tree --name-only --full-name -r $CI_COMMIT_SHA)
errors=""
badFiles=""
notValidated=""
servererrors=false
returnCode=0
#previousFiles=$(cat src/w3capi/revalidate.txt)
#files=$files$previousFiles

falsePositiveCount=0
for file in $files; do

    if [ ${file##*.} = "html" ]; then
        # header="\"Content-Type: text/html; charset=utf-8\""
        echo "$file is html"
    elif [ ${file##*.} = "css" ]; then
        # header="\"Content-Type: text/css; charset=utf-8\""
        echo "$file is css"
    else
        echo "skipping ${file}"
        continue
    fi

    # Wait for a second as requested by the API documentation
    sleep 1

    if [[ -e success ]]; then
        cache=$(cat success)
        alreadyValid=false
        for cachedFile in $cache; do
            echo "Reading cached file: $cachedFile"
            if [ $file = $cachedFile ]; then
                alreadyValid=true
                for committedFile in $commitFiles; do
                    if [ $file = $committedFile ]; then
                        alreadyValid=false
                    fi
                done
            fi
        done

        if [ "$alreadyValid" = true ]; then
            echo "Skipping $file, reason: already validated"
            continue
        fi
    fi

    echo "Sending ${file} to the validator"

    # headCmd="curl --silent -H ${header} -I ${api}"
    # respCmd="curl --silent -H ${header} --data-binary @$file ${api}?out=$out" ##Encoding problem
    # respCmd="curl --silent -H ${header} -F uploaded_file=@${file} ${api}"
    # response=$(curl -s -D - -H ${header} -F uploaded_file=@${file} ${api})
    response=$(curl -s -D - -F uploaded_file=@${file} ${api})
    # status=$(${headCmd} | grep "HTTP/2" | grep -o -e "\s[0-9]*\s" | grep -o -e "[0-9][0-9]*")
    status=$(echo "${response}" | grep "HTTP/2" | grep -o -e "\s[0-9]*\s" | grep -o -e "[0-9][0-9]*")

    if [[ $status -ne "200" ]]; then
        echo "ERROR: response status code is ${status}"
        servererrors=true
        notValidated="${notValidated}${file}\n"
        continue
    fi

    ##echo $response
    ##errors=${errors}$(echo ${response} | grep "\"type\":\"error\"") ##Om JSON hade fungerat...
    echo "$response" >.resp
    sed -e '1,/^\r\{0,1\}$/d' -e 's/ /_/g' -e 's/<\/li>/<\/li>\n/g' -e 's/<li/\n<li/g' -e 's/<ol>/\n/g' .resp >.resp.sed

    response=$(cat .resp.sed)
    fileErrors=$(echo "${response}" | grep "class=\"error\"")
    fileWarnings=$(echo "${response}" | grep "class=\"info_warning\"")

    errorCount=0
    vueErrorCount=0
    warnings=0
    if [[ $fileErrors != "" ]]; then
        for error in $fileErrors; do
            errLine=$(echo "$error" | grep "not_allowed_as_child_of_element")
            if [ errLine == "" ]; then
                badFiles="${badFiles}${file}\n"
                errorCount=$(($errorCount + 1))
            else
                vueErrorCount=$(($vueErrorCount + 1))
            fi
        done
        if [ $vueErrorCount -gt 0 ]; then
            echo "Vue-related errors found in $file, ignoring them"
        fi
    fi
    if [[ $fileWarnings != "" ]]; then
        for warning in $fileWarnings; do
            warnings=$(($warnings + 1))
        done

        if [ $warnings -gt 0 ]; then
            echo "WARNING: found $warnings warnings in $file"
            warnFiles="${warnFiles}${file}\n"
        fi
    fi

    if [ $errorCount -eq 0 ]; then
        if [ $warnings -eq 0 ]; then
            echo "$file passed validation"
            # echo "caching $file"
            # echo -e "$file\n" >> success
        else
            echo "$file has no errors, but has $warnings warnings"
        fi
    else
        echo "$file has $errorCount errors and $warnings warnings"
    fi

    if [ $vueErrorCount -gt 0 ]; then
        echo "Found $vueErrorCount false positives in $file"
    fi

    falsePositiveCount=$(($falsePositiveCount+$vueErrorCount))
done

if [[ $badFiles != "" ]]; then
    echo "ERRORS DETECTED!"
    echo "The validator found errors in:"
    echo -e "$badFiles"
    echo "Please validate them manually at $api"
    returnCode=1
else
    echo "OK: found no errors in HTML/CSS"
    echo "OK: found $falsePositiveCount false positives"
    if [ "$warnFiles" != "" ]; then
        echo "WARNING: There are warnings in: "
        echo -e "$warnFiles"
    fi
    if [ "$servererrors" = true ]; then
        echo "WARNING: unable to validate the following files:"
        echo -e "$notValidated"
        echo "Please validate them manually at $api"
    fi
    returnCode=0
fi

#echo "" > src/w3capi/revalidate.txt
#echo -e "$badFiles" >> src/w3capi/revalidate.txt
#echo -e "$notValidated" >> src/w3capi/revalidate.txt

#pwd

#cat src/w3capi/revalidate.txt

exit $returnCode
